Systemd-Resolved
=========

Enable systemd-resolved

Role Variables
--------------

  overwrite_etc_resolv_conf: if set to true overwrites /etc/resolv.conf, if false, fails if a file /etc/resolv.conf is already present.

License
-------

MIT
